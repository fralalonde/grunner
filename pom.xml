<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>ca.rbon</groupId>
	<artifactId>grunner</artifactId>
	<packaging>jar</packaging>
	<name>grunner</name>
	<version>0.2-SNAPSHOT</version>

	<properties>
		<java.version>14</java.version>
		<springfox-version>2.9.2</springfox-version>
		<org.mapstruct.version>1.4.0.Final</org.mapstruct.version>
		<db.schema>GRUNNER</db.schema>
		<db.url>jdbc:h2:${project.build.outputDirectory}/${db.schema}</db.url>
		<db.username>sa</db.username>
	</properties>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.3.4.RELEASE</version>
	</parent>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.openapitools</groupId>
				<artifactId>openapi-generator-maven-plugin</artifactId>
				<version>4.3.1</version>
				<executions>
					<execution>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<inputSpec>${project.basedir}/src/main/resources/api.yaml</inputSpec>
							<generatorName>spring</generatorName>
							<generateApiTests>false</generateApiTests>
							<generateModelTests>false</generateModelTests>
							<configOptions>
								<useOptional>true</useOptional>
								<!--
								this service does not require async, keeping it simple
								<reactive>true</reactive>
								-->
								<swaggerDocketConfig>true</swaggerDocketConfig>
								<dateLibrary>java8</dateLibrary>
								<interfaceOnly>true</interfaceOnly>
								<skipDefaultInterface>true</skipDefaultInterface>
								<apiPackage>ca.rbon.grunner.api</apiPackage>
								<modelPackage>ca.rbon.grunner.api.model</modelPackage>
								<configPackage>ca.rbon.grunner.api.config</configPackage>
							</configOptions>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.flywaydb</groupId>
				<artifactId>flyway-maven-plugin</artifactId>
				<version>6.5.3</version>

				<executions>
					<execution>
						<phase>generate-sources</phase>
						<goals>
							<goal>migrate</goal>
						</goals>
					</execution>
				</executions>

				<configuration>
					<url>${db.url}</url>
					<user>${db.username}</user>
					<cleanOnValidationError>true</cleanOnValidationError>
					<locations>
						<location>filesystem:src/main/resources/db/migration</location>
					</locations>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.jooq</groupId>
				<artifactId>jooq-codegen-maven</artifactId>
				<version>3.13.4</version>

				<executions>
					<execution>
						<phase>generate-sources</phase>
						<goals>
							<goal>generate</goal>
						</goals>
					</execution>
				</executions>

				<configuration>
					<jdbc>
						<url>${db.url}</url>
						<user>${db.username}</user>
					</jdbc>
					<generator>
						<generate>
							<javaTimeTypes>true</javaTimeTypes>
						</generate>
						<database>
							<includes>.*</includes>
							<inputSchema>${db.schema}</inputSchema>
						</database>
						<target>
							<packageName>ca.rbon.grunner.db</packageName>
							<directory>target/generated-sources/jooq-h2</directory>
						</target>
					</generator>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.1</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
					<showDeprecation>true</showDeprecation>
					<compilerArgs>
						<!-- enable all default warnings -->
						<arg>-Xlint</arg>
						<!-- Verbose compiler messages -->
						<arg>-Xdiags:verbose</arg>
						<!-- treat warnings as errors : NOTE this is hard, javac is picky -->
						<!--<arg>-Werror</arg>-->
						<!-- disable anachronic serialization warnings -->
						<arg>-Xlint:-serial</arg>
						<!-- disable misguided annotation warnings -->
						<arg>-Xlint:-processing</arg>
						<!-- disable warning about boostrap class path not set in conjunction with -source 1.7 -->
						<arg>-Xlint:-options</arg>
						<!-- disable pedantic try-with-resource unreferenced warnings -->
						<arg>-Xlint:-try</arg>
					</compilerArgs>
					<annotationProcessorPaths>
						<path>
							<groupId>org.mapstruct</groupId>
							<artifactId>mapstruct-processor</artifactId>
							<version>${org.mapstruct.version}</version>
						</path>
					</annotationProcessorPaths>
				</configuration>
			</plugin>

			<plugin>
				<groupId>net.revelc.code.formatter</groupId>
				<artifactId>formatter-maven-plugin</artifactId>
				<version>2.12.1</version>
				<configuration>
					<configFile>${basedir}/formatter.xml</configFile>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.22.0</version>
				<configuration>
					<excludedGroups>ca.rbon.grunner.Integration</excludedGroups>
				</configuration>
			</plugin>

			<plugin>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>2.22.0</version>
				<configuration>
					<includes>
						<include>**/*</include>
					</includes>
					<groups>ca.rbon.grunner.Integration</groups>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>integration-test</goal>
							<goal>verify</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>dockerfile-maven-plugin</artifactId>
				<version>1.4.13</version>
				<executions>
					<execution>
						<id>default</id>
						<goals>
							<goal>build</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<repository>grunner</repository>
					<tag>${project.version}</tag>
					<buildArgs>
						<JAR_FILE>${project.build.finalName}.jar</JAR_FILE>
					</buildArgs>
				</configuration>
			</plugin>

			<plugin>
				<groupId>net.revelc.code</groupId>
				<artifactId>impsort-maven-plugin</artifactId>
				<version>1.4.1</version>
				<configuration>
					<removeUnused>true</removeUnused>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.codehaus.groovy</groupId>
				<artifactId>groovy-all</artifactId>
				<version>3.0.6</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>


	<dependencies>
		<!-- dev & test dependencies -->

		<dependency>
		  <!-- enable traces in HTTP responses in dev -->
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<optional>true</optional>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- feature compile dependencies -->
		<dependency>
			<groupId>org.codehaus.groovy</groupId>
			<artifactId>groovy-jsr223</artifactId>
		</dependency>

		<dependency>
			<groupId>org.mapstruct</groupId>
			<artifactId>mapstruct</artifactId>
			<version>${org.mapstruct.version}</version>
		</dependency>

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<version>1.4.199</version>
		</dependency>

		<dependency>
			<groupId>org.flywaydb</groupId>
			<artifactId>flyway-core</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jooq</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>

		<!-- copied from openapi-codegen -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${springfox-version}</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${springfox-version}</version>
		</dependency>
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.2.11</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-jsr310</artifactId>
		</dependency>
		<dependency>
			<groupId>org.openapitools</groupId>
			<artifactId>jackson-databind-nullable</artifactId>
			<version>0.1.0</version>
		</dependency>
		<!-- Bean Validation API support -->
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
		</dependency>
	</dependencies>
</project>
